# TypeScript WebViewer Basic App Tutorial

In this tutorial we will write a small WebViewer application using TypeScript and Visual Studio Code.

## Prerequisites
You need the following software installed and configured:

- [Visual Studio Code](https://code.visualstudio.com/)
- [Node.js](https://nodejs.org)

## Setup

1. Clone this repository: `git clone https://bitbucket.org/techsoft3d/ts-webviewer-basic-app.git`
2. Copy the following files from the HOOPS Communicator package into this tutorial project:
    * `web_viewer/src/js/hoops_web_viewer.js`
    * `web_viewer/src/js/engine-wasm.js`
    * `web_viewer/src/js/engine-asmjs.js`
    * `web_viewer/src/js/engine.easm`
    * `web_viewer/typescript/hoops_web_viewer.d.js`
    * `web_viewer/typescript/tcc.d.js`
3. Run the `start_server` script located in the `quick_start` folder of the HOOPS Communicator package
4. Open this tutorial project in Visual Studio Code

## Configuration

The `package.json` file provided with this tutorial project has dependencies that need to be installed. Open a terminal window in Visual Studio Code and run the following scripts:

1. Install dependencies: `npm install`
2. Start the project: `npm start`

Point your browser to [http://localhost:8080/index.html](http://localhost:8080/index.html) to view the tutorial project in its current state. For now, this page will show very little. Let's fix that.

## Create a Viewer with TypeScript

Open the file `index.ts` and add the following code:

```typescript
//! [create_viewer] 
var viewer: Communicator.WebViewer = null; 

async function createViewer() { 
    viewer = new Communicator.WebViewer({ 
        model: "microengine", 
        containerId: "viewer", 
        endpointUri: "ws://localhost:11180", 
    }); 

    viewer.start(); 
} 

$(() =>{ 
    createViewer(); 
}); 
//! [create_viewer] 
```

As you are typing out the code above, you will notice that Visual Studo Code's IntelliSense offers code completion hints and code assistance as you type. Also, it will highlight any errors that you make.

## Build TypeScript

Before we can see our code in action, we need to compile it into JavaScript. Run the following commands to produce the compiled JavaScript, source maps, and view the results:

1. Press `Ctrl + Shift + B` in Visual Studio Code and select `tsc:build` from the menu
    * Alternatively, you can run `npm run build` from your terminal
2. Reload your browser

After reloading, the `microengine` model provided in the HOOPS Communicator package will appear on your screen.

## Adding a Simple Callback

Before we are done, let us add a callback to our WebViewer, which will write the name of the selected node to the page. Add the following function to `index.ts`:

```typescript
//! [on_selection] 
function onSelection(selectionEvents: Communicator.Event.NodeSelectionEvent[]) : void { 
    let $selectionBox = $("#selected-part"); 
 
    if (selectionEvents.length > 0) { 
        let selection = selectionEvents[0]; 
        let name = viewer.model.getNodeName(selection.getSelection().getNodeId()); 
        $selectionBox.html(`Selected Part: ${name}`); 
    }else { 
        $selectionBox.html("Selected Part: None"); 
    } 
} 
//! [on_selection] 
```

This function will get the name of the node that was selected, and update the HTML accordingly. The final step is to bind this function to the WebViewer instance. Add the following code right above the call to `viewer.statrt()` in `createViewer()`:

```typescript
//! [set_callbacks] 
viewer.setCallbacks({ 
    selectionArray: onSelection 
}); 
//! [set_callbacks] 
```

At this point, if you build the TypeScript code and reload the browser you will notice the text on the page update as you select different parts of the `micronengine` model.

## Wrapping Up

In this tutorial we have seen how to set up our editor for HOOPS Communicator development with TypeScript. We have also seen how TypeScript can make the development process faster and less error-prone thanks to Visual Studio Code's IntelliSense.